from fastapi import FastAPI, HTTPException

app = FastAPI()

# Base de datos temporal (en la memoria)
peliculas_db = [
    {"id": 1, "titulo": "Talk to me", "director": "Director", "anio": 2020},
    {"id": 2, "titulo": "Saltburn", "director": "Director", "anio": 2021},
    {"id": 3, "titulo": "Sociedad de la nieve", "director": "Director", "anio": 2022},
]

@app.get("/")
def read_root():
    return {"message": "API de Películas"}

@app.get("/peliculas/{pelicula_id}")
def read_pelicula(pelicula_id: int):
    pelicula = next((p for p in peliculas_db if p["id"] == pelicula_id), None)
    if pelicula:
        return pelicula
    else:
        raise HTTPException(status_code=404, detail="Película no encontrada")

@app.post("/peliculas/")
def create_pelicula(pelicula: dict):
    nueva_pelicula = {
        "id": len(peliculas_db) + 1,
        "titulo": pelicula["titulo"],
        "director": pelicula["director"],
        "anio": pelicula["anio"],
    }

    peliculas_db.append(nueva_pelicula)

    return {"message": "Película creada correctamente", "pelicula": nueva_pelicula}

